
//Criação e tipos de variáveis 

let num : number = 10;
let bool : boolean = true;
let str1 : string = "A beautiful message";
let str2 : string = 'A beautiful message';

//var str3 : string = `${str2} ola 
//outra mensagem`; // funciona...


//void é considerado um tipo primitivo no typescript
function func(): void{}

if(bool != null){}

if(bool != undefined) {}

if (bool) {
    let i: number; //let é um tipo de variavel que morre deentro do escopo, o var não
    for(i = 0; i < 3; i++){
    }
}

//criando arrays de duas formas 
let list1: number[] = [1,2,3];
let list2: Array<number> = [1,2,3];
//fim

//array tipo tuple
let tuple: [string, number];
tuple = ["william", 22];
// tuple =  [22, "william"]; //não funciona..
// tuple = ["william", 22, "jacob", "jason"]; //funciona..
//console.log(tuple[0].toLowerCase());


//outro tipo, enumeração
enum Day{MONDAY = 1, TUESDAY, }; //dando um valor para o Monday os subsequentes vão ter valores de acordo do o monday em ordeem crescente
//enum Day{MONDAY, TUESDAY = 0}; //resultado: todos os valores vão ser zeros
let day: Day = Day.MONDAY;
console.log(`day:  ${ day } | ${Day.TUESDAY} | ${Day[1]}`);