//Criação e tipos de variáveis 
var num = 10;
var bool = true;
var str1 = "A beautiful message";
var str2 = 'A beautiful message';
//var str3 : string = `${str2} ola 
//outra mensagem`; // funciona...
//void é considerado um tipo primitivo no typescript
function func() { }
if (bool != null) { }
if (bool != undefined) { }
if (bool) {
    var i = void 0; //let é um tipo de variavel que morre deentro do escopo, o var não
    for (i = 0; i < 3; i++) {
    }
}
//criando arrays de duas formas 
var list1 = [1, 2, 3];
var list2 = [1, 2, 3];
//fim
//array tipo tuple
var tuple;
tuple = ["william", 22];
// tuple =  [22, "william"]; //não funciona..
// tuple = ["william", 22, "jacob", "jason"]; //funciona..
//console.log(tuple[0].toLowerCase());
//outro tipo, enumeração
var Day;
(function (Day) {
    Day[Day["MONDAY"] = 1] = "MONDAY";
    Day[Day["TUESDAY"] = 2] = "TUESDAY";
})(Day || (Day = {}));
; //dando um valor para o Monday os subsequentes vão ter valores de acordo do o monday em ordeem crescente
var day = Day.MONDAY;
console.log("day:  " + day + " | " + Day.TUESDAY + " | " + Day[1]);
//# sourceMappingURL=variables.js.map